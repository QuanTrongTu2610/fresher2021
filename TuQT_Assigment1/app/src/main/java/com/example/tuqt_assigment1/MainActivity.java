package com.example.tuqt_assigment1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /*
    * declare views components in main activity
    * */
    private Button btn_toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // call reflect to bind View
        reflect();

        // add event listener for Toast button
        btn_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Xin Chao", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void reflect() {
        btn_toast = findViewById(R.id.btn_toast);
    }
}